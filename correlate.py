from math import *

def mean(values):
	return sum(values) / len(values)

def rms(values):
	v_bar = mean(values)
	sigma = 0
	for v in values:
		sigma += (v - v_bar)**2
	return sqrt(sigma / len(values))

def covariance(x, y):
	assert len(x) == len(y)
	x_bar = mean(x)
	y_bar = mean(y)

	values = [((x_v - x_bar) * (y_v - y_bar)) for x_v, y_v in zip(x, y)]
	return mean(values)

def correlate(x, y):
	return covariance(x, y) / rms(x) / rms(y)

def line_of_best_fit(x, y):
	assert len(x) == len(y)
	x_bar = mean(x)
	y_bar = mean(y)

	m = sum((x_v - x_bar)*(y_v - y_bar) for x_v, y_v in zip(x, y)) / sum((x_v - x_bar)**2 for x_v in x)
	b = y_bar - m*x_bar

	return (m, b)


if __name__ == "__main__":
	values1 = 13,23,12,44,55
	values2 = 13,23,12,44,55

	# print(mean(values1))
	# print(rms(values1))
	# print(covariance(values1, values2))
	# print(correlate(values1, values2))
	# print(line_of_best_fit(values1, values2))

	import random

	a = [random.randint(0, 100000) for i in range(1004)]
	b = [random.randint(0, 100000) for i in range(1004)]

	print(correlate(a, b))
