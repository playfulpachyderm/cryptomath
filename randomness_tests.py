from collections import defaultdict
import random import SystemRandom	

def get_random_bytes(n):
	random = SystemRandom()
	return sorted([random.randrange(n) for i in range(n)])

def dist(samples, space):
	b = defaultdict(int)
	for i in range(samples):
		b[len(set(get_random_bytes(space)))] += 1
	return b

def print_dist(dist):
	for i in range(min(dist), max(dist) + 1):
		print i, dist[i]

def avg(samples, space):
	d = dist(samples, space)
	return sum([i * d[i] for i in range(min(d), max(d) + 1)]) / float(samples)

def x(samples, space):
	return avg(samples, space) / space
