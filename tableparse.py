from bs4 import BeautifulSoup
import re


class Table(object):
	def __init__(self, table):
		self.soup = BeautifulSoup(table, "html.parser")
		self.headers = []
		self.data = []
		self.parse()

	def parse(self):
		for header in self.soup.thead.find_all("th"):
			self.headers.append(header.text.replace("\n", " "))

		for row in self.soup.tbody.find_all("tr"):
			r = []
			for d in row.find_all("td"):
				text = d.text.replace(",", "").replace("%", "")
				try:
					text = float(text)
				except ValueError:
					pass
				r.append(text)
			self.data.append(r)

	def print_headers(self):
		for h in self.headers:
			print(h)

	def print_data(self):
		for r in self.data:
			for d in r:
				print(d, end = "\t")
			print()

	def get_column(self, n):
		return [r[n] for r in self.data]

	def get_column_numeric(self, n):
		return [i for i in self.get_column(n) if type(i) in (float, int)]

if __name__ == "__main__":
	try:
		text = open("table").read()
	except UnicodeDecodeError:
		import codecs
		text = open("table", encoding = "utf-8").read().encode("utf-8")

	table = Table(text)
	# print(text)

	from correlate import *
	from collections import defaultdict
	years = table.get_column_numeric(4)
	years = [int(i - min(years)) for i in years]
	deaths = table.get_column_numeric(6)

	by_shooting = years, deaths
	by_year = [0]*int(max(years) + 1)

	for d, y in zip(deaths, years):
		by_year[y] += d

	by_year = list(range(len(by_year))), by_year


	print(by_year)
	average_deaths_by_shootings = sum(by_shooting[1]) / max(by_shooting[0])
	average_deaths_by_year = sum(by_year[1]) / max(by_year[0])
	assert average_deaths_by_year == average_deaths_by_shootings
	print("Average deaths per year: ", average_deaths_by_shootings)

	print(line_of_best_fit(*by_year))
	print(correlate(*by_year))


Read more: http://louderwithcrowder.com/7-year-old-girl-embarrasses-anti-gun-reporter/#ixzz4BtxuP7V6
Follow us: @scrowder on Twitter | stevencrowderofficial on Facebook
