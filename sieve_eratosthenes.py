def sieve(n):
	a = [True]*n
	for i in range(2, n/2):
		if not a[i]:
			continue
		for j in range(2*i, n, i):
			a[j] = False
	return a
