from math import factorial, e, pi, log, sqrt

def approximate(f1, f2, n1, n2):
	for n in range(n1, n2):
		print("{}: {}, {}".format(n, f1(n)/f2(n), f1(n)-f2(n)))

def stirling(n):
	return round(sqrt(2*pi*n)*(n/e)**n)

def o(n):
	return e**(n*log(n)-n)*log(n)**2

log_factorial = lambda n: log(factorial(n))
log_o = lambda n: log(o(n))
