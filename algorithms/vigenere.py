## Symmetric key
## Uses ASCII encoding.

def get_key():
	return raw_input("Enter key\n> ")

ASCII_CONSTANT = 128

def E(plaintext, key):
	ciphertext = ""

	for i, v in enumerate(plaintext):
		value = (ord(v) + ord(key[i % len(key)])) % ASCII_CONSTANT
		ciphertext += chr(value)

	return ciphertext


def D(ciphertext, key):
	plaintext = ""

	for i, v in enumerate(ciphertext):
		value = (ord(v) - ord(key[i % len(key)])) % ASCII_CONSTANT
		plaintext += chr(value)

	return plaintext
