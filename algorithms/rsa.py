import math
import random

def get_key():
	raise NotImplementedError
	#return raw_input("Enter key\n> ")

def miller_rabin_base(n, d, s, a):
	"""
	Returns False if a proves n is not prime, True otherwise
	"""
	if modular_exp(a, n, d) == 1:
		return True
	for r in range(s+1):
		if modular_exp(a, n, 2**r * d) == n-1:
			return True
	return False

def SnD(n):
	n_1 = n-1
	s = 0
	while not (n_1 & 1):
		s += 1
		n_1 >>= 1

	d = n_1

	return s, d

def miller_rabin(n, k = 10):
	s, d = SnD(n)

	for a in [2, 3, 5, 7, 11, 13, 17]:
		if a >= n:
			break
		if not miller_rabin_base(n, d, s, a):
			return False

	for i in range(k):
		r = random.SystemRandom()
		a = r.getrandbits(n.bit_length() - 1)
		if not miller_rabin_base(n, d, s, a):
			return False

	return True

def prime_gen(bit_length, k = 10):
	r = random.SystemRandom()
	base = r.getrandbits(bit_length - 1)
	prime_candidate = (base << 1) + 1
	attempts = 1
	while not miller_rabin(prime_candidate, k):
		prime_candidate += 2
		attempts += 1
	return prime_candidate, attempts

def EEA(x, y):
	"""
	Return a tuple (GCD, a, b), where ax + by = GCD
	"""

	r = [x, y]
	q = [0, 0]
	a = [1, 0]
	b = [0, 1]

	while r[-1] != 0:
		q.append(r[-2] // r[-1])
		r.append(r[-2] % r[-1])
		a.append(a[-2] - a[-1] * q[-1])
		b.append(b[-2] - b[-1] * q[-1])

	# GCD, a, b
	# Such that:
	# ax + by = 0
	return r[-2], a[-2], b[-2]

# Modular Multiplicative Inverse
def MMI(x, m):
	GCD, a, q = EEA(x, m)
	if GCD != 1:
		raise ValueError("Invalid value {} for mod {} (not co-prime, thus MMI does not exist)".format(x, m))

	return a % m

def keygen(p, q):
	"""
	Return public, private: (n, e), (n, d)
	"""
	n = p * q
	totient_n = (p - 1) * (q - 1)  # works since both p and q are prime
	
	for e in [17, 2**16+1]:
		try:
			d = MMI(e, totient_n)
			return (n, e), (n, d)
		except ValueError:
			pass

	raise ValueError("Couldn't find a good public exponent for input primes {}, {}".format(p, q))

def modular_exp(base, mod, exp):
	ret = 1
	e = 1
	value = base % mod
	while e <= exp:
		if e & exp:
			ret = (ret * value) % mod
		e = e << 1
		value = (value ** 2) % mod

	return ret

def to_msg(s):
	msg = 0
	for i in s:
		msg <<= 8
		msg += ord(i)

	return msg

def from_msg(msg):
	msg_len = msg.bit_length()

	mask = (1 << 8) - 1

	s = ""
	s_len = int(math.ceil(float(msg_len) / 8))
	for i in range(s_len):
		s += chr(msg & mask)
		msg >>= 8

	return s[::-1]

def base_n(msg, n):
	msg_len = msg.bit_length()

	blocks = []

	while msg > 0:
		blocks.append(msg % n)
		msg //= n

	return blocks[::-1]

def base_10(blocks, n):
	msg = 0
	for block in blocks:
		msg *= n
		msg += block

	return msg

def encrypt_blocks(blocks, key):
	return [modular_exp(block, *key) for block in blocks]

def E(plaintext, key):
	n, e = key

	blocks = base_n(to_msg(plaintext), n)
	encrypted_blocks = [modular_exp(block, n, e) for block in blocks]
	
	return from_msg(base_10(encrypted_blocks, n))

def D(ciphertext, key):
	return E(ciphertext, key)

s = "The quick brown fox jumped over the lazy dog!!!!"

public = 3233, 17
private = 3233, 2753

blocks = base_n(to_msg(s), public[0])

















