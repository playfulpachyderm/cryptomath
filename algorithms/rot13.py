#!/usr/bin/python

## Symmetric key
## (technically.  There is no key for rot13, but both E and D use the same no-key.)

def get_key():
	return None

def E(plaintext, _ = ""):  # no key!  (not very secure)
	d = {}
	for case in (ord('a'), ord('A')):
		for i in range(26):
			d[chr(case + i)] = chr(case + ((i + 13) % 26))

	return "".join([d.get(i, i) for i in plaintext])

def D(ciphertext, _ = ""):
	# The magic of rot13!
	return E(ciphertext)

if __name__ == "__main__":
	import sys

	if not sys.stdin.isatty():
		plaintext = sys.stdin.read()
	else:
		plaintext = "\n".join(sys.argv[1:])
		if not plaintext[-1] == "\n":
			plaintext += "\n"

	sys.stdout.write(E(plaintext))
