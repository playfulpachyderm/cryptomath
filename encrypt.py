#!/usr/bin/python

import sys
import os
import argparse
from importlib import import_module

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--decrypt", action = "store_true")
parser.add_argument("algo")
parser.add_argument("filename")

args = parser.parse_args()

plaintext = open(args.filename).read()

algo_module = import_module("algorithms.{}".format(args.algo))
key = algo_module.get_key()

if args.decrypt:
	ciphertext = getattr(algo_module, "D")(plaintext, key)
	out_file = open("{}.decrypt".format(args.filename), 'w')
else:
	ciphertext = getattr(algo_module, "E")(plaintext, key)
	out_file = open("{}.{}".format(args.filename, args.algo), 'w')

out_file.write(ciphertext)
out_file.close()
