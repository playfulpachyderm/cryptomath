from math import sqrt

def nPr(n, r):
	ret = 1
	for i in range(n, n-r, -1):
		ret *= i
	return ret

def nCr(n, r):
	ret = nPr(n, r)
	for i in range(r, 0, -1):
		ret /= i
	return ret

# n people, H days in a year
# (choose n from sample size H)
def birthday(n, H):
	not_birthday = 1.0
	for i in range(H, H-n, -1):
		not_birthday *= i
		not_birthday /= H
	return 1 - not_birthday

def size_for_collision(H):
	n = 1
	while birthday(n, H) < 0.5:
		n += 1
	return (n, n/sqrt(H))

def test():
	n, k = size_for_collision(365)
	assert n == 23, n

if __name__ == "__main__":
	for H in [3, 10, 33, 100, 333, 1000, 3333, 10000, 33333, 100000, 333333, 1000000, 3333333, 10000000]:
		print(size_for_collision(H))
